/**
 * Enum contain unit with its value per one meter.
 * @author Prin Angkunanuwat
 *
 */
public enum Area implements Unit {
	
	SQUAREMETER("Square Meter",1.00), 
	SQUARECENTIMETER("Square Centimeter",1.0E-4), 
	SQUAREKILOMETER("Square Kilometer",1.0E6), 
	SQUAREMILLIMETER("Square Millimeter",1.0E-6), 
	ACRE("Acre",4047.0), 
	ROOD("Rood",1012.0),
	PERCH("Perch",25.29),
	RAI("Rai",1600.0),
	NGAN("Ngan",400.0);
	
	public final String name;
	public final double value;
	/**
	 * Constructor for enum.
	 * @param name is name of unit.
	 * @param value is value of unit per one meter.
	 */
	private Area( String name, double value){
		this.name = name;
		this.value = value;
	}
	
	@Override
	/**
	 * Convert amount from this unit to other unit.
	 * @param amt is amount to convert.
	 * @param unit after converted.
	 * @return amount after converted.
	 */
	public double convertTo(double amt, Unit unit) {
		return ( amt * value ) / unit.getValue();
	}

	@Override
	/**
	 * Get value of unit per one meter.
	 * @return value per one meter.
	 */
	public double getValue() {
		return value;
	}
	/**
	 * Describe Unit.
	 * @return description of unit.
	 */
	public String toString(){
		return name;
	}


}
