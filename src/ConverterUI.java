import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ConverterUI is ui for unit converter.
 * @author Prin Angkunanuwat
 *
 */
public class ConverterUI extends JFrame 
{
	// attributes for graphical components
	private JButton convertButton;
	private JTextField inputField1;
	private JTextField inputField2;
	private JComboBox<Unit> comboLeft;
	private JComboBox<Unit> comboRight;
	private JButton clearButton;
	private UnitConverter unitconverter;
	private JMenu menu;
	private JMenuBar menuBar;

	/**
	 * Construct and call initComponents.
	 * @param uc is unitConverter used to convert value.
	 */
	public ConverterUI( UnitConverter uc ) {
		this.unitconverter = uc;
		this.setTitle("Unit Converter");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(700, 100);
		this.setResizable(false);
		initComponents( );
	}

	/**
	 * initialize components in the window
	 */
	private void initComponents() {

		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout( );
		contents.setLayout( layout );

		convertButton = new JButton("Convert");
		inputField1 = new JTextField(10);
		inputField2 = new JTextField(10);
		comboLeft = new JComboBox<Unit>(unitconverter.getUnits(UnitType.Length));
		comboRight = new JComboBox<Unit>(unitconverter.getUnits(UnitType.Length));
		clearButton = new JButton("Clear");
		JLabel label = new JLabel("=");


		menu = new JMenu("Unit Type");
		UnitType[] utype = UnitType.values();
		for(int i = 0 ; i < utype.length ; i++){
			menu.add( new JMenuItem(utype[i].toString()) );
			menu.getItem(i).addActionListener(new MenuListener());
		}
		menu.addSeparator();
		menu.add( new ExitAction() );


		menuBar = new JMenuBar();
		menuBar.add(menu);
		this.setJMenuBar(menuBar);

		contents.add( inputField1 );
		contents.add( comboLeft );
		contents.add( label );
		contents.add( inputField2 );
		contents.add( comboRight );
		contents.add( convertButton );
		contents.add( clearButton );

		ActionListener listener = new ConvertButtonListener( );
		ActionListener clear = new ClearButtonListener( );

		clearButton.addActionListener( clear );
		convertButton.addActionListener( listener );
		inputField1.addActionListener( listener );
		inputField2.addActionListener( listener );


		this.setVisible(true);
	}



	/**
	 * ActionListener get value from text1 or text2  
	 * and convert it between unit in combobox1 or combobox2.
	 * 
	 *
	 */
	class ConvertButtonListener implements ActionListener {

		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {


			String s1 = inputField1.getText().trim();
			String s2 = inputField2.getText().trim();

			Unit unit1 =  (Unit) comboLeft.getSelectedItem();
			Unit unit2 =  (Unit) comboRight.getSelectedItem();
			
			boolean success = false;
			if(evt.getSource() == convertButton){
				if ( s1.length() > 0 || s2.length() > 0) {
					try{ 
						double value = Double.valueOf( s1 );
					

						double result = unitconverter.convert(value, unit1, unit2);
						inputField2.setText(String.format("%.5g", result));
						success = true;
					} catch (NumberFormatException e){
						try{
							double value = Double.valueOf( s2 );
							

							double result = unitconverter.convert(value, unit2, unit1);
							inputField1.setText(String.format("%.5g", result));
							success = true;
						} catch (NumberFormatException e2){}
					}
				}
					
				
			} else if(evt.getSource() == inputField1){
				if ( s1.length() > 0 ) {
					try{ 
						double value = Double.valueOf( s1 );
					

						double result = unitconverter.convert(value, unit1, unit2);
						inputField2.setText(String.format("%.5g", result));
						success = true;
					} catch (NumberFormatException e){}
				}
			} else if(evt.getSource() == inputField2){
				if ( s2.length() > 0) {
					try{
						double value = Double.valueOf( s2 );

						double result = unitconverter.convert(value, unit2, unit1);
						inputField1.setText(String.format("%.5g", result));
						success = true;
					} catch (NumberFormatException e2){}
				}
			}
			if( success ){
				inputField2.setForeground(Color.black);
				inputField1.setForeground(Color.black);
			} else {
				inputField2.setForeground(Color.red);
				inputField1.setForeground(Color.red);				
			}
		}
	} 
	/**
	 * Clear all text to empty String.
	 * 
	 *
	 */
	class ClearButtonListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			inputField1.setText("");
			inputField2.setText("");
		}
	}
	/**
	 * Change unit in combobox to  unit in menu.
	 * 
	 *
	 */
	class MenuListener implements ActionListener {
		public void actionPerformed( ActionEvent evt ){
			JMenuItem itype = (JMenuItem) evt.getSource();
			UnitType utype = UnitType.valueOf(itype.getText()) ;
			DefaultComboBoxModel<Unit> Model1 = new DefaultComboBoxModel<Unit>(unitconverter.getUnits(utype));
			DefaultComboBoxModel<Unit> Model2 = new DefaultComboBoxModel<Unit>(unitconverter.getUnits(utype));
			comboLeft.setModel(Model1);
			comboRight.setModel(Model2);
		}
	}
	/**
	 * Exit the program.
	 * 
	 *
	 */
	class ExitAction extends AbstractAction {
		public ExitAction( ) {
			super("Exit");
		}
		public void actionPerformed(ActionEvent evt) {
			System.exit(0);
		}
	}



}
