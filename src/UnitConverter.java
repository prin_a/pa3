import java.util.Arrays;

/**
 * The UnitConverter class receives requests from the UI 
 * to converts a value from one unit to another. 
 * @author Prin Angkunanuwat
 *
 */
public class UnitConverter {
	/**
	 * Convert amount from one unit to other unit.
	 * @param amount is a value wanted to convert.
	 * @param fromUnit is the unit before convert.
	 * @param toUnit is the unit wanted to convert to.
	 * @return amount after convert to toUnit.
	 */
	public double convert(double amount, Unit fromUnit, Unit toUnit) {
		return fromUnit.convertTo(amount, toUnit);
	}
	/**
	 * Get all constant in enum.
	 * @return an array of unit contains all constant in enum.
	 */
	public Unit[] getUnits(UnitType utype) {
		String type = utype.toString();
		if(type.equals("Length")) return Length.values();
		if(type.equals("Area"))   return Area.values();
		if(type.equals("Weight")) return Weight.values();
		return Volume.values();
	}
	
	
	
	
	
}
