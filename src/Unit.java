/**
 * Interface that have three method.
 * @author Print Angkunanuwat
 *
 */
public interface Unit {
	/**
	 * Convert amount from this unit to other unit.
	 * @param amt is amount to convert.
	 * @param unit after converted.
	 * @return amount after converted.
	 */
	double convertTo( double amt, Unit unit );
	/**
	 * Get value of unit per one meter.
	 * @return value per one meter.
	 */
	double getValue();
	/**
	 * Describe Unit.
	 * @return description of unit.
	 */
	String toString();
	
}
