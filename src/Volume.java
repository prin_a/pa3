/**
 * Enum contain unit with its value per one meter.
 * @author Prin Angkunanuwat
 *
 */
public enum Volume implements Unit {
	
	CUBICMETER("Cubic Meter",1.00), 
	CUBICCENTIMETER("Cubic Centimeter",1.0E-6), 
	CUBICKILOMETER("Cubic Kilometer",1.0E9), 
	CUBICMILLIMETER("Cubic Millimeter",1.0E-9), 
	GALLON("Gallon",4.546 * 1.0E-3), 
	BARREL("Barrel",0.1636),
	QUART("Quart",1.137 * 1.0E-3),
	KWIAN("Kwian",2),
	THANG("Thang",0.02);
	
	public final String name;
	public final double value;
	/**
	 * Constructor for enum.
	 * @param name is name of unit.
	 * @param value is value of unit per one meter.
	 */
	private Volume( String name, double value){
		this.name = name;
		this.value = value;
	}
	
	@Override
	/**
	 * Convert amount from this unit to other unit.
	 * @param amt is amount to convert.
	 * @param unit after converted.
	 * @return amount after converted.
	 */
	public double convertTo(double amt, Unit unit) {
		return ( amt * value ) / unit.getValue();
	}

	@Override
	/**
	 * Get value of unit per one meter.
	 * @return value per one meter.
	 */
	public double getValue() {
		return value;
	}
	/**
	 * Describe Unit.
	 * @return description of unit.
	 */
	public String toString(){
		return name;
	}


}
