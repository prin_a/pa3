/**
 * Enum contain unit with its value per one meter.
 * @author Prin Angkunanuwat
 *
 */
public enum Weight implements Unit {
	
	GRAM("Gram",1.00), 
	MICROGRAM("Microgram",1.0E-6), 
	KILOGRAM("Kilogram",1000.0), 
	MILLIGRAM("Milligram",0.001), 
	STONE("Stone",6350.0), 
	GRAIN("Grain",0.0648),
	POUND("Pound",453.6),
	TAMLUENG("Tamlueng",60.98),
	CHANG("Chang",1220.0);
	
	public final String name;
	public final double value;
	/**
	 * Constructor for enum.
	 * @param name is name of unit.
	 * @param value is value of unit per one meter.
	 */
	private Weight( String name, double value){
		this.name = name;
		this.value = value;
	}
	
	@Override
	/**
	 * Convert amount from this unit to other unit.
	 * @param amt is amount to convert.
	 * @param unit after converted.
	 * @return amount after converted.
	 */
	public double convertTo(double amt, Unit unit) {
		return ( amt * value ) / unit.getValue();
	}

	@Override
	/**
	 * Get value of unit per one meter.
	 * @return value per one meter.
	 */
	public double getValue() {
		return value;
	}
	/**
	 * Describe Unit.
	 * @return description of unit.
	 */
	public String toString(){
		return name;
	}
	

}
