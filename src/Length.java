/**
 * Enum contain unit with its value per one meter.
 * @author Prin Angkunanuwat
 *
 */
public enum Length implements Unit {
	
	METER("Meter",1.00), 
	CENTIMETER("Centimeter",0.01), 
	KILOMETER("Kilometer",1000.0), 
	MILE("Mile",1609.344), 
	FOOT("Foot",0.30480), 
	WA("Wa",2.0),
	INCH("Inch",0.0254),
	YARD("Yard",3 * 0.30480),
	MICRON("Micron",1.0E-6);
	
	public final String name;
	public final double value;
	/**
	 * Constructor for enum.
	 * @param name is name of unit.
	 * @param value is value of unit per one meter.
	 */
	private Length( String name, double value){
		this.name = name;
		this.value = value;
	}
	
	@Override
	/**
	 * Convert amount from this unit to other unit.
	 * @param amt is amount to convert.
	 * @param unit after converted.
	 * @return amount after converted.
	 */
	public double convertTo(double amt, Unit unit) {
		return ( amt * value ) / unit.getValue();
	}

	@Override
	/**
	 * Get value of unit per one meter.
	 * @return value per one meter.
	 */
	public double getValue() {
		return value;
	}
	/**
	 * Describe Unit.
	 * @return description of unit.
	 */
	public String toString(){
		return name;
	}
	

}
