import java.util.Arrays;

/**
 * Main class run the UI.
 * @author Prin Angkunanuwat
 *
 */
public class Main {
	/**
	 * Main method create ui and converter.
	 * @param args is not used.
	 */
	public static void main(String[] args){
		UnitConverter uc = new UnitConverter();
		ConverterUI ui = new ConverterUI(uc);
	}
}